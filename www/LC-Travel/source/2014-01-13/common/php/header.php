<div id="sectionHeaderWrap">
    <div id="sectionHeaderIn">
        <div id="logo">
            <h1>
                <a href="<?php echo WEB_ROOT ?>">
                	<img src="<?php echo WEB_ROOT ?>common/img/header/logo.gif" width="185" height="65" class="linkfade"/>
                </a>
            </h1>
        </div>
        <div id="gNavi">
            <ul>
                <li class="home">
                    <a href="<?php echo WEB_ROOT ?>">
                        <img src="<?php echo WEB_ROOT ?>common/img/header/home.gif" width="63" height="65" class="normal fadechange"/>
                        <img src="<?php echo WEB_ROOT ?>common/img/header/home-hover.gif" width="63" height="65" />
                    </a>
                </li>
                <li class="about">
                    <a href="<?php echo WEB_ROOT ?>about/index.html">
                        <img src="<?php echo WEB_ROOT ?>common/img/header/about.gif" width="85" height="65" class="normal fadechange"/>
                        <img src="<?php echo WEB_ROOT ?>common/img/header/about-hover.gif" width="85" height="65"/>
                    </a>
                </li>
                <li class="tutorial">
                    <a href="<?php echo WEB_ROOT ?>tutorials/index.html">
                        <img src="<?php echo WEB_ROOT ?>common/img/header/tutorial.gif" width="71" height="65" class="normal fadechange"/>
                        <img src="<?php echo WEB_ROOT ?>common/img/header/tutorial-hover.gif" width="71" height="65"/>
                    </a>
                </li>
                <li class="portfolio">
                    <a href="<?php echo WEB_ROOT ?>portfolio/index.html">
                        <img src="<?php echo WEB_ROOT ?>common/img/header/portfolio.gif" width="76" height="65" class="normal fadechange"/>
                        <img src="<?php echo WEB_ROOT ?>common/img/header/portfolio-hover.gif" width="76" height="65" />
                    </a>
                </li>
                <li class="contact">
                    <a href="<?php echo WEB_ROOT ?>contact/index.html">
                        <img src="<?php echo WEB_ROOT ?>common/img/header/contact.gif" width="73" height="65" class="normal fadechange"/>
                        <img src="<?php echo WEB_ROOT ?>common/img/header/contact-hover.gif" width="73" height="65" />
                    </a>
                </li>
            </ul>
        </div>
        
    <!-- /sectionHeaderIn --></div>
<!-- /sectionHeaderWrap --></div>