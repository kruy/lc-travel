$(window).load(function() {
	$('.imglist img').each(function() {
		$(this).wrap('<div style="display:inline-block;width:' + this.width + 'px;height:' + this.height + 'px;">').clone().addClass('gotcolors').css({'position': 'absolute', 'opacity' : 0 }).insertBefore(this);
		this.src = grayscale(this.src);
	}).animate({opacity: 1}, 500);
});
$(document).ready(function($) {

	$(".linkfade").hover(function(){
		$(this).stop().animate({"opacity" : 0.5},300);
		},function(){
		$(this).stop().animate({"opacity" : 1},300);
	});
	
	$(".fadechange").hover(function(){
		$(this).stop().animate({"opacity" : 0},500);
		},function(){
		$(this).stop().animate({"opacity" : 1},500);
	});
	
	$(".imglist a").hover(
		function() {
			$(this).find('div').css({'position': 'relative','overflow': 'hidden'}); // do this in your stylesheet
			var nativeImage = new Image();
			nativeImage.src = this.getElementsByTagName('img')[0].src;
			$(this).find('.gotcolors').stop().css({'height': 0,'width': nativeImage.width,'top': nativeImage.height}).stop().animate({opacity: 1,top: 0,height: nativeImage.height}, 200);
		}, 
		function() {
			$(this).find('.gotcolors').animate({opacity: 0}, 1000);
		}
	);
	
	//Slider effect
	$('.imgSlider').cycle({ 
		fx:    'fade', 
		pause:  1 ,
		next:   '.next', 
    	prev:   '.pre' 
	});
	
	var slideIndex = 0;
	var nextIndex = 0;
	var prevIndex = 0;
	$('.specialPackimgSlider').cycle({ 
		fx:    'scrollLeft',
		timeout: 0,
		speed: 1000,
		after: function(currSlideElement, nextSlideElement, options) {
			slideIndex = options.currSlide;
			nextIndex = slideIndex + 1;
			prevIndex = slideIndex -1;
	
			if (slideIndex == options.slideCount-1) {
				nextIndex = 0;
			}
	
			if (slideIndex == 0) {
				prevIndex = options.slideCount-1;
			}
		},
		before: function(){
			var title = $(this).attr('title');
			var description = $(this).attr('alt');
        	$('.title').html(title);
			$('.description').html(description);
			
		}
	});
	$(".next1").click(function () {
		$('.specialPackimgSlider').cycle(nextIndex, "scrollRight");
	});
	
	$(".pre1").click(function () {
		$('.specialPackimgSlider').cycle(prevIndex, "scrollLeft");
	});
	
	//Reservation Menu
	$('.reservation-menu li img').click(function(){		
		var getAlt = $(this).attr("alt");
		$('.reservation-menu li img').removeClass('curren');
		$(this).addClass('curren');
		
		if( getAlt == "flight"){
			$('.formWrap').css('display','none');
			$('.flight-reservation').css('display','block').hide().fadeIn();
		}
		if( getAlt == "car"){
			$('.formWrap').css('display','none');
			$('.car-reservation').css('display','block').hide().fadeIn();
		}
		if( getAlt == "hotel"){
			$('.formWrap').css('display','none');
			$('.hotel-reservation').css('display','block').hide().fadeIn();
		}
	});
	$( ".lcDatePicker" ).datepicker();
	/*$( ".datepicker" ).datepicker({
		inline: true,
		showOtherMonths: true
	});*/
});

// http://net.tutsplus.com/tutorials/javascript-ajax/how-to-transition-an-image-from-bw-to-color-with-canvas/
function grayscale(src) {
	var supportsCanvas = !!document.createElement('canvas').getContext;
	if (supportsCanvas) {
		var canvas = document.createElement('canvas'), 
		context = canvas.getContext('2d'), 
		imageData, px, length, i = 0, gray, 
		img = new Image();
		
		img.src = src;
		canvas.width = img.width;
		canvas.height = img.height;
		context.drawImage(img, 0, 0);
			
		imageData = context.getImageData(0, 0, canvas.width, canvas.height);
		px = imageData.data;
		length = px.length;
		
		for (; i < length; i += 4) {
			gray = px[i] * .3 + px[i + 1] * .59 + px[i + 2] * .11;
			px[i] = px[i + 1] = px[i + 2] = gray;
		}
				
		context.putImageData(imageData, 0, 0);
		return canvas.toDataURL();
	} else {
		return src;
	}
}
